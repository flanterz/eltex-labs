#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#define R 7
#define STR_MAX_SIZE 10

/* #1 Generate random string  */
char *rand_string(char *str, size_t size) {
    srand(time(NULL));
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if(size) {
        for(size_t n = 0; n < (size-1); n++) {
            int key = rand() % (int) (sizeof charset - 1);
            str[n] = charset[key];
        }
        str[size] = '\0';
    }
    return str;
}

/* #2 Generate n-count files and writes the result to the specified array of strings  */
void fileGeneration(char **arr, int countOfFiles) {
    int fd;
    srand(time(NULL));
    /* Creating files */
    for(int i=0; i<countOfFiles; i++) {

        /* Filename generating */
        char *tempName = malloc(sizeof(char)*8); // 255 is MAX size of filename in ext4
        tempName[0] = 'f';
        tempName[1] = 48+i;
        tempName[2] = '\0';
        strcat(tempName,".txt");

        /* Write result to arr  */
        int k = 0;
        while(1) {
            arr[i][k] = tempName[k];
            if(tempName[k] == '\0') break;
            k++;
        }

        /* File create  */
        fd = creat(tempName, 0644);
        printf("File [%s] created\n",tempName );

        /* Fill file content  */
        int strSize = rand()%STR_MAX_SIZE;
        char* fileStr =  malloc(sizeof(char)*STR_MAX_SIZE);
        for(int j=0; j<strSize; j++ ) {
            char* tempchar = malloc(sizeof(char));
            rand_string(tempchar,strSize);
            strcat(fileStr, tempchar);
        }

        write(fd, fileStr, strlen(fileStr));
        close(fd);
    }
    return;
}


int main(int argc, char** argv) {
    int n,i,fd,status,stat;
    char buff[100];
    printf("Введите кол-во процессов: ");
    scanf("%d", &n);
    pid_t pid[n];
    int pp[n][2];
    char **filesArr = (char **)malloc(sizeof(char *)*n);
    for(i=0; i<n; i++) {
        filesArr[i] = (char *)malloc(sizeof(char)*80);
    }
    fileGeneration(filesArr,n);
    for (int i = 0; i < n; i++) {
        pipe(pp[i]);
        char *tempName;
        pid[i] = fork();
        if(pid[i] == -1) perror ("fork"), exit(EXIT_FAILURE);
        if(pid[i] == 0) {
            struct flock flptr;
            int v=0;
            while(1) {
                tempName = malloc(sizeof(char)*R);
                tempName[0] = 'f';
                tempName[1] = 48+v;
                tempName[2] = '\0';
                strcat(tempName,".txt");
                fd = open(filesArr[v], O_RDWR|O_EXCL);
                fcntl(fd, F_GETLK, &flptr);
                if(flptr.l_type==F_UNLCK) {
                    /* Unlocked  */
                    if(lockf(fd, F_LOCK, 0) >= 0) {
                        printf("Запись:%s заблокирована\n", tempName);
                        fflush(stdout);
                        break;
                    } else {
                        v++;
                        if(v>n) v=0;
                    }
                } else if(flptr.l_type==F_RDLCK) {
                    /* Read block  */
                    v++;
                    if(v>n) v=0;
                } else if(flptr.l_type==F_WRLCK) {
                    /* Write block  */
                    v++;
                    if(v>n) v=0;
                }

            }
            sleep(rand()%5);
            read(fd, buff, 64);
            int checkSum = 0;
            for(int y=0; buff[y]!='\0'; y++) {
                checkSum+=buff[y];
            }
            if(lockf(fd, F_ULOCK, 0) < 0) perror("F_SETLOCK");
            else printf("Запись:%s разблокирована [%d]\n", tempName,getpid());
            fflush(stdout);
            close(fd);

            close(pp[i][0]);
            write(pp[i][1], &checkSum, sizeof(int));
            //printf("Процесс (%d) посчитал контрольную сумму [%d] файла %s\n", getpid(),checkSum,tempName);
            fflush(stdout);
            exit(0);
        }
    }

    for (i = 0; i < n; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(stat));
            /* процесс-родитель закрывает доступный для записи конец канала 1*/
            close(pp[i][1]);
            /* читает из канала 0*/
            int cs = 0;
            read(pp[i][0], &cs, sizeof(int));
            //printf("i=%d len=%d\n", i, len );
            printf("Процесс (%d) посчитал контрольную сумму [%d]\n", i,cs);
        }
    }
    return 0;
}
