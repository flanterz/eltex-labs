#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    int n,i,gold,t;
    printf("Введите кол-во юнитов, золота:\n");
    scanf("%d %d", &n, &gold);
    int fdOne[n][2],fdTwo[n][2];
    pid_t cpid;
    for(i=0; i<n; i++) {

        /* Creating Pipes and FORK */
        if (pipe(fdOne[i]) == -1) {
            perror("pipe");
            exit(EXIT_FAILURE);
        }
        if (pipe(fdTwo[i]) == -1) {
            perror("pipe");
            exit(EXIT_FAILURE);
        }
        cpid = fork();
        if (cpid == -1) {
            perror("fork");
            exit(EXIT_FAILURE);
        }
        /* Creating Pipes and FORK*/

        if (cpid == 0) {
            while(gold > 0) {
                close(fdOne[i][1]);
                read(fdOne[i][0], &gold, sizeof(int));
                //sleep(rand()%3);
                t = gold-1;
                close(fdTwo[i][0]);
                write(fdTwo[i][1], &t, sizeof(int));
                if(t<=-1) {
                  exit(0);
                }
                printf("Юнит (s%d) добыл 1 единиц золота. Остаток: %d\n", getpid(), t);
                sleep(rand()%4);

            }
        }
    }
    while(gold > 0) {
        for (i = 0; i < n; i++) {
            close(fdOne[i][0]);
            write(fdOne[i][1], &gold, sizeof(int));
            close(fdTwo[i][1]);
            read(fdTwo[i][0], &t, sizeof(int));
            if(t>=0)
              gold = t;
        }
    }
    return 0;

}
