#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h>
#include <string.h>
#include <string.h>
#define MAX_LEN 1024
int counter=0;
/*
  Расположить строки в алфавитном порядке
  Входные параметры:
  1. Массив
  2. Размерность массива
  Выходные параметры:
  1. Количество перестановок
  2. Первая буква первой строки
*/
char** readMas(int count){
	char buffer[MAX_LEN];
	char **mas; //указатель на массив указателей на строки
	mas = (char **)malloc(sizeof(char *)*count); // выделяем память для массива указателей
    for (int i = 0; i < count ; i++){
        scanf("%s", buffer);
        mas[i] = (char *)malloc(sizeof(char)*strlen(buffer)); //выделяем память для строки
        strcpy(mas[i], buffer);
    }
    return mas;
}

void printMas(char **mas, int count){
    printf("\nArray of strings:\n");
    for (int i = 0; i < count ; i++){
        printf("%s\n", mas[i]);
    }
}

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
        free(mas[i]);
    }
    free(mas);
}

char** bubbleSort(char **array, int array_size){
	int i = 0;
	char* buf;
	char swap_cnt = 0;
	if (array_size == 0)
		return array;
	while (i < array_size) {
		if (i + 1 != array_size && strlen(array[i]) > strlen(array[i + 1])) {
			buf = array[i];
			array[i] = array[i + 1];
			array[i + 1] = buf;
			swap_cnt = 1;
			counter++;
		}
		i++;
		if (i == array_size && swap_cnt == 1){
			swap_cnt = 0;
			i = 0;
		}
	}
	return array;
}
int main(int argc, char **argv){
  int i,n;
	char **mas = NULL;
	scanf("%d", &n);
	//printf("Adress: %d Value: %d\n",&n, n );
	mtrace();
	mas = readMas(n);
  mas = bubbleSort(mas,n);
	printMas(mas, n);
	printf("Counter: %d\n", counter);
	freeMas(mas, n);

  return 0;
}
