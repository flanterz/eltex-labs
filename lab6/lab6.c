#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
char DataFile [] = "file.txt";
char info     [] = "99";
int main(int argc, char** argv) {
    char buff[64];
    int n,i, work, status, stat,count,fd,gold;
    printf("Введите кол-во юнитов: ");
    scanf("%d", &n);
    printf("Сколько они добывают золота: ");
    scanf("%d", &work);
    printf("\n");
    pid_t pid[n];
    fd = creat(DataFile, 0644);
    write(fd, info, strlen(info));
    close(fd);
    fd = open("file.txt", O_RDWR|O_EXCL);
    count = read(fd, buff, 64);
    gold = atoi(buff);
    close(fd);
    for (int i = 0; i < n; i++) {
        pid[i] = fork();
        if(pid[i] == -1) perror ("fork"), exit(EXIT_FAILURE);
        if(pid[i] == 0) {
            while(gold > 0) {
                fd = open("file.txt", O_RDWR|O_EXCL);
                if(lockf(fd, F_LOCK, 0) < 0)  perror("SETLOCK");
                sleep(2);
                count = read(fd, buff, 64);
                gold = atoi(buff);
                gold -= work;
                sprintf(buff, "%d", gold);
                lseek(fd, 0, 0);
                write(fd,buff,count);
                if(lockf(fd, F_ULOCK, 0) < 0) perror("F_SETLOCK");
                close(fd);
                printf("Юнит (%d)[%d] вышел из шахты, добыл %d единиц золота | В шахте осталось %d\n", getpid(), getppid(), work,gold);
                sleep(rand()%5);
            }
        }
    }

    for (i = 0; i < n; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) printf("Процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(stat));
    }
    return 0;
}
