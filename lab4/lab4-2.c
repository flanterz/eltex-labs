#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mcheck.h>
/*
Заменить цифры на пробелы
Параметры командной строки:
	1. Имя входного файла
	2. Количество замен
*/
int mainCnt=0;
void get_file_strings(char **str, FILE *fp, int n){
  int i=0,j=0,m,flag=0,off=0;
  char s;
  char *temp;
  while((s=fgetc(fp)) != EOF) {
    temp[0]=s;
    for(m = 0; m < 10; m++)
      if(strchr(temp,m+47) != NULL) flag = 1;
    if(flag == 1 && off!=1) s = ' ';
    str[i][j] = s;
    j++; flag = 0;
    if(s == '\n') {
      i++;
      j=0;
      mainCnt++;
    }
    if(mainCnt >= n-1) off=1;
  }
}

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
        free(mas[i]);
    }
    free(mas);
}

int main(int argc, char *argv[]){
  short flag = 0;
	FILE *fp,*stdout;
  char* str = argv[1],strTwo[sizeof(argv[1])+4];
  int n = atoi(argv[2]),i,j,z=100; // Count of strings
  char** strArray;

  /* #1 File open checking & cout of argumets */
	if (argc < 2){
		fprintf (stderr, "Мало аргументов. Используйте <имя файла> <строка>\n");
		exit (1);
	}
	if((fp=fopen(argv[1], "r"))==NULL) {
		printf("Не удается открыть файл.\n");
		exit(1);
	}
  /* #1 File open checking & cout of argumets */

  /* #2 Memory allocation and Tracing */
  mtrace();
  strArray = (char**)malloc(sizeof(char*)*200);
  for (i = 0; i < 6; i++){
      strArray[i] = (char*)malloc(sizeof(char)*10);
  }
	get_file_strings(strArray, fp, n);
  /* #3 Memory allocation and Tracing */

  /* #4 Changing file format */
  for(i = 0;i<sizeof(str);i++) {
    strTwo[i] = str[i];
  }
  strcat(strTwo,".txt");
  /* #4 Changing file format */

  /* #5 Opening second file to write and writing into this, closing */
  if((stdout=fopen(strTwo, "w"))==NULL) {
		printf("Не удается открыть файл %s\n",strTwo);
		exit(1);
	}
  char* temp;
  for(i = 0; i < mainCnt; i++) {
    temp = strArray[i];
    while(*temp){
      if(!ferror(stdout)){
        fputc(*temp++, stdout);
      }
    }
  }

	if(fclose(stdout)){
		printf("Ошибка при закрытии файла %s\n",strTwo);
		exit(1);
	}
  /* #5 Opening second file to write and writing into this, closing */
  freeMas(strArray,n);
   return 0;
}
