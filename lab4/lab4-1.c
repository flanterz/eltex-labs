#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mcheck.h>
/*
Оставить строки, не содержащие цифры
Параметры командной строки:
	1. Имя входного файла
	2. Количество обрабатываемых строк
*/

void get_file_strings(char **str, FILE *fp, int n){
  int i=0;
  while( i<n && !feof (fp) ){
    fgets(str[i],19,fp);
    i++;
  }
}

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
        free(mas[i]);
    }
    free(mas);
}

int main(int argc, char *argv[]){
  short flag = 0;
	FILE *fp,*stdout;
  char* str = argv[1],strTwo[sizeof(argv[1])+4];
  int n = atoi(argv[2]),i,j; // Count of strings
  char** strArray;

  /* #1 File open checking & cout of argumets */
	if (argc < 2){
		fprintf (stderr, "Мало аргументов. Используйте <имя файла> <строка>\n");
		exit (1);
	}
	if((fp=fopen(argv[1], "r"))==NULL) {
		printf("Не удается открыть файл.\n");
		exit(1);
	}
  /* #1 File open checking & cout of argumets */

  /* #2 Memory allocation and Tracing */
  mtrace();
  strArray = (char**)malloc(sizeof(char*)*n);
  for (i = 0; i < n ; i++){
      strArray[i] = (char*)malloc(sizeof(char)*20);
  }
	get_file_strings(strArray, fp, n);
  /* #3 Memory allocation and Tracing */

  /* #4 Changing file format */
  for(i = 0;i<sizeof(str);i++) {
    strTwo[i] = str[i];
  }
  strcat(strTwo,".txt");
  /* #4 Changing file format */

  /* #5 Opening second file to write and writing into this, closing */
  if((stdout=fopen(strTwo, "w"))==NULL) {
		printf("Не удается открыть файл %s\n",strTwo);
		exit(1);
	}

  for(i = 0; i < n; i++) {
    for(j = 0; j < 10; j++)
      if(strchr(strArray[i],j+47) != NULL) flag = 1;
    if(!flag) fputs(strArray[i],stdout);
    flag = 0;
  }

	if(fclose(stdout)){
		printf("Ошибка при закрытии файла %s\n",strTwo);
		exit(1);
	}
  /* #5 Opening second file to write and writing into this, closing */
  freeMas(strArray,n);
   return 0;
}
