#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>

//extern int sum(int x, int y);
//extern int minus(int x, int y);
int main(int argc, char* argv[]) {
  void *ext_library;
  double (*powerfunc)(int x,int y);
  ext_library = dlopen("/home/mak/Рабочий стол/eltex-labs/lab5/libdyn.so",RTLD_LAZY);
	if (!ext_library){
		//если ошибка, то вывести ее на экран
		fprintf(stderr,"dlopen() error: %s\n", dlerror());
		return 1;
	};
  //загружаем из библиотеки требуемую процедуру
	powerfunc = dlsym(ext_library, argv[1]);

	//выводим результат работы процедуры
	printf("%s(3,2) = %d\n",argv[1],(*powerfunc)(3,2));

	//закрываем библиотеку
	dlclose(ext_library);
  //printf("Summa = %d, minus = %d\n", sum(1,2),minus(3,2));
  return 0;
}
