#include <stdio.h>
#include <stdlib.h>
/*
Фамилия спортсмена
Вид спорта
Количество медалей
Расположить в алфавитном порядке записи с ненулевым количеством медалей
*/
int count;
struct sportsman {
    char surname[50];
    char type[50];
    int medals;
};

static int cmp(const void *p1, const void *p2) {
    struct sportsman* st1 = *(struct sportsman**)p1;
    struct sportsman* st2 = *(struct sportsman**)p2;
    return st1->medals - st2->medals;
}

int main(){

    printf("Введите кол-во спортсменов: ");
    scanf("%d", &count);
    struct sportsman** st = malloc(sizeof(struct sportsman*)*count);
    printf("\n");
    for (int i = 0; i < count ; i++) {
        st[i] = malloc (sizeof(struct sportsman));
        printf("Введите фамилию: ");
        scanf("%s", st[i]->surname);
        printf("Введите вид спорта: ");
        scanf("%s", st[i]->type);
        printf("Кол-во медалей: ");
        scanf("%d", &st[i]->medals);
        printf("\n");
    }

    qsort(st, count, sizeof(struct st*), cmp);
    for (int i = 0; i < count; i++) {
      printf("\n\nФамилия студента: %s\nВид спорта: %s \nКол-во медалей: %d", st[i]->surname,
            st[i]->type,st[i]->medals);
            free(st[i]);
    }

    free(st);
    return 0;
}
